working-bootloader-image.hex:
- I took a board with an unknown program on it
- I used AS6.0 with PDI programmer
- I erased application memory block
- I took image of MCU memory, this hex file is the entire memory block


Honeycomb2-RED-xboot.hex:
- first working version, compiled from command line!
- red blinking LED


Honeycomb2-GREEN-xboot.hex:
- LED pin changed in the config file "x128a3.conf.mk"
- working version, compiled from command line
- green blinking LED

